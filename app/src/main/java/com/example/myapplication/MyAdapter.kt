package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView

class MyAdapter : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    class MyViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var first: TextView
        private var second1: TextView
        private var second2: TextView
        private var third: TextView

        private var divider: View

        init {
            first = itemView.findViewById(R.id.first)
            second1 = itemView.findViewById(R.id.second1)
            second2 = itemView.findViewById(R.id.second2)
            third = itemView.findViewById(R.id.third)
            divider = itemView.findViewById(R.id.divider)
        }

        fun onBind(model: Model) {
            first.setTextOrHide(model.first)
            second1.setTextOrHide(model.second)
            second2.setTextOrHide(model.second)
            third.setTextOrHide(model.third)
            divider.isVisible = model.third != null
        }
    }
}

data class Model(
    val first: String,
    val second: String? = null,
    val third: String? = null
)

val data = listOf(
    Model("First", "Second", "Third"),
    Model("First", third = "Third"),
    Model("First", "Second"),
    Model("First"),
)

fun TextView.setTextOrHide(value: String?) {
    isVisible = value != null
    text = value
}